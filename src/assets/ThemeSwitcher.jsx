import React, { useContext } from "react";
import ThemeContext from '../context/ProfileContext';
import { Button } from 'react-bootstrap';

function ThemeSwitcher() {
    const {theme, setTheme} = useContext(ThemeContext);

    return (
        <Button variant="dark" style={{color:"whitesmoke", marginLeft: "1em", boxShadow: "3px 3px 2px rgba(46, 46, 46, 0.62)"}}
        onClick = {() => setTheme( theme == 'dark' ? 'light' : 'dark')}
        className='mx-3'>Change Theme
        </Button>
    )
}

export default ThemeSwitcher;