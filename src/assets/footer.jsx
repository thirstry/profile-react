import React from "react";
import '../App.css';

const Footer = () => {
  return (
    <div className="footer">
      <p>&copy;2021 Mahathir Muhammad</p>
    </div>
  );
}

export default Footer;