import React from 'react';
import '../App.css';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faGitlab, faGithub, faLinkedin, faInstagram } from '@fortawesome/free-brands-svg-icons';

const Icon = () => {
    return ( 
        <div className="icon">
            <a href= "https://gitlab.com/thirstry">
                <h2><FontAwesomeIcon icon ={faGitlab} style={{fontSize: "1.5em", color: "whitesmoke"}}/></h2>
            </a>
            <a href= "https://github.com/thirstry">
                <h2><FontAwesomeIcon icon ={faGithub} style={{fontSize: "1.5em", color: "whitesmoke"}}/></h2>
            </a>
            <a href= "https://www.linkedin.com/in/maha-athir">
                <h2><FontAwesomeIcon icon ={faLinkedin} style={{fontSize: "1.5em", color: "whitesmoke"}}/></h2>
            </a>
            <a href="https://www.instagram.com/maha.athir">
                <h2><FontAwesomeIcon icon={faInstagram} style={{fontSize: "1.5em", color: "whitesmoke"}}/></h2>
            </a>
        </div>
       
    )
}
export default Icon;
