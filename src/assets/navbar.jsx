import React, { useContext } from "react";
import '../App.css';
import { Navbar, Nav, Form, FormControl, Button } from "react-bootstrap";
import ThemeContext from '../context/ProfileContext';
import ThemeSwitcher from './ThemeSwitcher';

const MyNavbar = () => {
  const { theme } = useContext(ThemeContext) 

  return (
    <>
    <Navbar bg={theme} variant={theme}>
        <Navbar.Brand style={{fontWeight:"bolder"}} href="/">Hi There!</Navbar.Brand>
        <Nav className="mr-auto">

        <Nav.Link activeStyle={{fontWeight:"bolder", color: "black"}}
          href="/profile">About</Nav.Link>

        <Nav.Link activeStyle={{fontWeight:"bolder", color: "black"}}
          href="/contact-me">Contact Me</Nav.Link>

        <Nav.Link activeStyle={{fontWeight:"bolder", color: "black"}}
          href="/todolist">Try ToDoList</Nav.Link>

        <Nav.Link activeStyle={{fontWeight:"bolder", color: "black"}}
          href="/user">User</Nav.Link>

        <Nav.Link activeStyle={{fontWeight:"bolder", color: "black"}}
          href="/post">Post</Nav.Link>

        </Nav>
        <ThemeSwitcher />

        <Form inline>
        <FormControl type="text" placeholder="Search" className="mr-sm-2" />
        <Button variant="outline-info">Search</Button>
        </Form>

    </Navbar>
    </>
  );
}

export default MyNavbar;