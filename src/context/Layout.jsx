import React, { useContext } from 'react';
import MyNavbar from '../assets/navbar';
import { Helmet } from 'react-helmet';
import ThemeContext from './ProfileContext';

const Layout = ({children}) => {
    const { theme } = useContext(ThemeContext)
    const bg =
    theme == 'dark'
        ? "body {background-color: #404042; color: rgb(212, 212, 212);}"
        : "body {background-color: #fff; color: #000}"

    return (
        <>
        <Helmet>
            <style>{bg}</style>
        </Helmet>
        <MyNavbar />
        {children}
        </>
  )
}

export default Layout;