import axios from 'axios';
import React, {useReducer} from 'react';
import { IconButton, List } from '@material-ui/core';
import './style.scss';


function AxiosCard() {

    const initialState = {user: []};

    function reducer(state, action) {
        switch (action.type) {
            case "posts":
                return {
                    ...state, 
                    user: action.payload
                };
            case "clear":
                return {
                    user: []
                }
                default:
                    return "error";
        }
    }

    const [state, dispatch] = useReducer(reducer, initialState);

    const reducerGet = () => {
        state.user.length > 0 ? dispatch({ type: "clear"}) : 

        axios
        .get("https://jsonplaceholder.typicode.com/posts")
        .then((res) => dispatch({ payload: res.data, type: "posts"}));
 
    }

    console.log(state.user);

    return (
    <div >
        <div className="post">
        <h1 style={{color: "whitesmoke", marginLeft: "2em"}}>Show Data API "Post"</h1>
        <ul>
        {state.user.map((data, i) => 
            <List key={i}>
                <p>UserId: {data.userId}</p>
                <p>Id: {data.id}</p>
                <p>Title: {data.title}</p>
                <p>Body: {data.body}</p>
            </List>
        )}
        </ul>
      <IconButton style={{marginLeft: "1.5em", marginTop: "-2em"}}onClick={reducerGet} cvariant="contained" color="secondary">Show</IconButton> 
        </div>

    </div>
  );
}

export default AxiosCard;