import axios from 'axios';
import React, {useReducer} from 'react';
import { IconButton, List } from '@material-ui/core';
import './style.scss';


function User() {

    const initialState = {user: []};

    function reducer(state, action) {
        switch (action.type) {
            case "user":
                return {
                    ...state, 
                    user: action.payload
                };
            case "clear":
                return {
                    user: []
                }
                default:
                    return "error";
        }
    }

    const [state, dispatch] = useReducer(reducer, initialState);

    const reducerGet = () => {
        state.user.length > 0 ? dispatch({ type: "clear"}) : 

        axios
        .get("https://jsonplaceholder.typicode.com/users")
        .then((json) => dispatch({ payload: json.data, type: "user"}));
 
    }

    console.log(state.user);

    return (
    <div >
        <div className="user">
        <h1 style={{color: "whitesmoke", marginLeft: "2em"}}>Most Wanted</h1>
        <ul>
        {state.user.map((data, i) => 
            <List key={i}>
                <p>Nama: {data.name}</p>
                <p>Email: {data.email}</p>
                <p>Address: 
                    {data.address.street} ,
                    {data.address.suite} ,
                    {data.address.zipcode}
                </p>
                <p>Phone: {data.phone}</p>
            </List>
        )}
        </ul>
      <IconButton style={{marginLeft: "1.5em", marginTop: "-2em"}}onClick={reducerGet} cvariant="contained" color="secondary">Show</IconButton> 
        </div>

    </div>
  );
}

export default User;