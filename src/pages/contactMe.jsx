import React, { useState } from "react";
import { Form, Button, Col, Modal } from "react-bootstrap";
import '../App.css';

function ContactMe() {
    const [ smShow, setSmShow ] = useState(false);
    
    return (
        <>
        <h1>Fill the form and get a free "basic-design" e-book!</h1>

        <div className="form">
            <Form>
            <Form.Row>
                <Form.Group as={Col} controlId="formGridName">
                <Form.Label>First Name</Form.Label>
                <Form.Control type="text" placeholder="Iron" />
                </Form.Group>

                <Form.Group as={Col} controlId="formGridName">
                <Form.Label>Last Name</Form.Label>
                <Form.Control type="text" placeholder="Man" />
                </Form.Group>
            </Form.Row>

            <Form.Group controlId="formGridEmail">
                <Form.Label>Email</Form.Label>
                <Form.Control type="email" placeholder="Enter Email" />
            </Form.Group>

            <Form.Group controlId="formGridPhone">
                <Form.Label>Phone</Form.Label>
                <Form.Control type="number" placeholder="62xxx xxxx xxxx" />
            </Form.Group>

            <Form.Group controlId="formGridCity">
                <Form.Label>City</Form.Label>
                <Form.Control type="text" placeholder="Bandung" />
            </Form.Group>

            <Form.Group>
                <Form.Check
                type="radio"
                label="Male"
                name="formHorizontalRadios"
                id="formHorizontalRadios1"
                />
                <Form.Check
                type="radio"
                label="Female"
                name="formHorizontalRadios"
                id="formHorizontalRadios2"
                />
            </Form.Group>

            <Form.Group controlId="formGridTextArea">
                <Form.Label>Say Hi (optional)</Form.Label>
                <Form.Control as="textarea" rows={3} placeholder="Type your message here" />
            </Form.Group>

            <Button style={{width:"100%"}} variant="outline-info" type="submit" onClick={() => setSmShow(true)}>
                Submit
            </Button>

            <div className="modal" style={{backgroundColor: "whitesmoke", color: "black"}}>
            <Modal
                size="sm"
                show={smShow}
                onHide={() => setSmShow(false)}
                aria-labelledby="example-modal-sizes-title-sm"
            >
                <Modal.Header closeButton>
                <Modal.Title id="example-modal-sizes-title-sm">
                    Thank You!
                </Modal.Title>
                </Modal.Header>
                <Modal.Body>
                <p>Please check your current email :)</p>
                </Modal.Body>
            </Modal>
            </div>

            </Form>
        </div>
        </>
    );
};

export default ContactMe;