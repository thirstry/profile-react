import React, { useContext } from "react";
import Footer from "../assets/footer";
import '../assets/landing.css';

import ThemeContext from '../context/ProfileContext';

const Landing = () => {
  const { theme } = useContext(ThemeContext)

  return (
    <>
    <div fluid className={theme}>
      <div className="athir-intro">
        <p style={{fontWeight: "800"}}>HI.. I'M MAHATHIR MUHAMMAD</p>
        <p style={{fontWeight: "650"}}>and you can call me ATHIR :)</p>
        <p style={{fontSize: "30px", fontWeight:"300", fontStyle: "italic"}}>*Pls kindly click on Navbar for more..</p>
      </div>
    </div>

    <Footer/>
    </>
  );
};

export default Landing;