import React from "react";

const NotFound = () => {
  return (
    <div>
      <h1 style={{textAlign: "center", margin: "1.5em 1em 1.5em 1em"}}>404 PAGE NOT FOUND</h1>
    </div>
  );
};

export default NotFound;