import React from "react";
import Icon from "../assets/icon";
import Footer from "../assets/footer";
import { Carousel } from 'react-bootstrap';
import Ava1 from "../assets/pict/ava.jpg";
import Ava2 from "../assets/pict/ava2.jpg";
import Ava3 from "../assets/pict/ava3.jpg";
import '../App.css';

const Profile = () => {
  return (
    <>
    <div className="athir-container">
        <div className="carousel">
        <Carousel fade>
            <Carousel.Item>
                <img
                className="d-block w-100"
                src={Ava1}
                alt="Pict 1"
                />
            </Carousel.Item>

            <Carousel.Item>
                <img
                className="d-block w-100"
                src={Ava2}
                alt="Pict 2"
                />
            </Carousel.Item>

            <Carousel.Item>
                <img
                className="d-block w-100"
                src={Ava3}
                alt="Pict 3"
                />
            </Carousel.Item>
        </Carousel>
        </div>
    
        <span>
        <p className="athir-p">So, welcome to my page!<br />
        I was born on December, 3rd 1996 in Makassar, South Sulawesi.
        And currently I lives in Makassar. I am a student of Glints Academy
        as Front-end Developer to be. I am passionate on visual-production,
        and have joy within play or listen some music in my spare time. <br />
        Just feel free to connect with me :D
        </p>
        <Icon />
        </span>

    </div>

    <Footer/>
    </>
  );
};

export default Profile;
