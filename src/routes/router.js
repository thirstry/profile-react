import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import Landing from "../pages/landing";
import Profile from "../pages/profile";
import ContactMe from "../pages/contactMe";
import NotFound from "../pages/notFound";
import MyNavbar from "../assets/navbar";
import ToDoList from "../todolist/ToDoList";
import Todo2 from "../todolist2/todo2";
import User from "../fetch/user";
import AxiosCard from '../fetch/post';

import { useState } from 'react';
import ThemeContext from '../context/ProfileContext';
import Layout from '../context/Layout';

import '../App.css';

const Routes = () => {
  const [theme, setTheme] = useState("light")
  const value = { theme, setTheme }

  return (
    <ThemeContext.Provider value={value}>
      <Layout>
        <Router>
          <Switch>
            <Route path="/" exact>
              <Landing />
            </Route>

            <Route path="/profile">
              <Profile />
            </Route>

            <Route path="/contact-me">
              <ContactMe />
            </Route>

            <Route path="/todolist">
              <ToDoList />
              {/* <Todo2 /> */}
            </Route>

            <Route path="/user">
              <User />
            </Route>

            <Route path="/post">
              <AxiosCard />
            </Route>

            <Route path="*">
              <NotFound />
            </Route>
          </Switch>
        </Router>
      </Layout>
    </ThemeContext.Provider>
  );
};

export default Routes;