import React, { useState } from "react";
import { FaEdit } from "react-icons/fa";
import PropTypes from "prop-types";

function AddToDo(props) {
  const [title, setTitle] = useState("");

  const onSubmit = (evt) => {
    // Stop form being submmited to same file and reloading the page
    evt.preventDefault();

    // Validate todo text
    if (!title) {
      alert("Please add a task description.");
      return;
    }

    props.addTodo(title);

    // Clear task text in component state
    setTitle("");
  };

  return (
    <div className="mx-4 mt-6">
      <form onSubmit={onSubmit} className="input-todo">
        <input
          name="task-title"
          type="text"
          placeholder="Add task..."
          value={title}
          onChange={(evt) => setTitle(evt.target.value)}
          className="flex-1 px-2.5 bg-gray-200 placeholder-gray-500 focus:outline-none"
          data-testid="task-input-field"
        />
        <button
          type="submit"
          className="transition duration-200 ease-in-out text-gray-400 focus:outline-none hover:text-pink-500 text-lg px-2 cursor-pointer"
          data-testid="task-submit-btn">
          <FaEdit />
        </button>
      </form>
    </div>
  );
}

AddToDo.propTypes = {
  addTodo: PropTypes.func.isRequired,
};

export default AddToDo;