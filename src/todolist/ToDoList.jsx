  
import React, { useState } from "react";
import { v4 as uuidv4 } from "uuid";
import AddToDo from "./AddToDo";
import ToDoFooter from "./ToDoFooter";
import ToDoHeader from "./ToDoHeader";
import ToDoItem from "./ToDoItem";
import './todo.css';

const ToDoList = () => {
  const [todos, setTodos] = useState([]);

  // Add a new todo item
  const addTodo = (title) => {
    let newTodo = {
      id: uuidv4(),
      title, // new in ES6: same as title: title
      completed: false,
    };

    // [...] = spread operator (copy items)
    // Used because we can't (and shouldn't) change state values directly
    setTodos([...todos, newTodo]);
  };

  // Delete a todo item
  const delTodo = (id) => {
    setTodos(todos.filter((todo) => todo.id !== id));
  };

  // Toggle completed state of todo item
  const markComplete = (id) => {
    setTodos(
      todos.map((todo) =>
        todo.id === id ? { ...todo, completed: !todo.completed } : todo
      )
    );
  };

  return (
    <div className="to-do-list">
    <div className="container">
      <ToDoHeader />

      <AddToDo addTodo={addTodo} />

      <div className="to-do-item">
        {todos.length > 0 ? (
          // If there are todo items, show them in a list
          <ul className="do-item" style={{marginTop: "1rem"}} data-testid="todos-list">
            {todos.map((todo) => (
              <ToDoItem
                key={todo.id}
                todo={todo}
                markComplete={markComplete}
                delTodo={delTodo}
              />
            ))}
          </ul>
        ) : (
          // No todo items, all caught up
          <p
            className="empty-todo"
            data-testid="empty-todos-message"
          >
            You're all caught up!
          </p>
        )}
      </div>
            
      <div className="to-do-footer">
        <ToDoFooter
            totalTasks={todos.length}
            doneTasks={todos.filter((todo) => todo.completed).length}
        />
      </div>

    </div>
    </div>
  );
}

export default ToDoList;