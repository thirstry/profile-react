import React, { useState } from "react";
import "./style.css";

function Todo2({ ...props }) {
  const { data } = props;

  const [inputs, setInputs] = useState({
    nama: "",
    kota: "",
  });
  const [save, setSave] = useState([]);
  const [id, setId] = useState(1);
  console.log(save);

  const eventInput = (event) => {
    setInputs({
      ...inputs,
      [event.target.name]: event.target.value,
    });
  };

  const setDone = (task) => {
    const select = save.find((selected) => selected.id === task.id);
    const notSelected = save.filter((noSelected) => noSelected.id !== task.id);
    select.done = !select.done;
    const newList = [...notSelected, select].sort(
      (min, max) => min.id - max.id
    );
    console.log(newList);
    setSave(newList);
  };

  const deleteItem = (item) => {
    const deleteTask = save.filter((selected) => selected.id !== item.id);
    setSave(deleteTask);
  };

  const evenClick = () => {
    console.log("data terkirim");
    setId(id + 1);
    setSave([...save, { name: inputs.nama, id: id, done: false }]);
  };

  return (
    <div>
      <h1>{data}</h1>

      <input type="text" placeholder="nama" onChange={eventInput} name="nama" />

      <button onClick={evenClick}>Submit</button>
      {save?.map((item) => {
        return (
          <div>
            <p className={item.done ? "todo-list-done" : "todo-list"}>
              {item.name}
            </p>
            <button onClick={() => setDone(item)}>Done</button>
            <button onClick={() => deleteItem(item)}>Delete</button>
          </div>
        );
      })}
    </div>
  );
}

export default Todo2;
